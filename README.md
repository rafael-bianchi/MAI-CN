# Complex Networks
This course is part of the Master in Artificial Intelligence @ UPC -> https://www.fib.upc.edu/en/studies/masters/master-artificial-intelligence/curriculum/syllabus/CN-MAI

### Course content
1. Introduction
Examples of complex networks in many knowledge fields. Complex network types.
2. Structure of complex network
Main topological and structural characteristics of complex networks: degree distribution, small-world, transitivity, assortativity, community structure, centrality. Community detection algorithms.
3. Complex network models
Erdös-Rényi random networks, Barabási-Albert model, Watts-Strogatz model, configuration model.
4. Dynamics on complex networks
Most important dynamics on complex networks: epidemic spreading, synchronization, diffusion, evolutionary games, percolation. Monte Carlo simulations. Phase transitions.
